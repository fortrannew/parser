import 'dart:async';
import 'dart:typed_data';
import 'package:intl/intl.dart';
import 'package:process_run/shell.dart';
import 'package:xml/xml.dart';
import 'dart:io';
import 'dart:isolate';
import '../modules/connect_db.dart';
import '../models/project.dart';
import '../models/parser.dart';
import '../pages/2parser/parser.dart';

class StreamWork {
  bool start = false;
}

StreamWork streamWork1 = StreamWork();

class ParserClass {
  Project project;
  List<Parser> parserArr;
  late Timer _timer;
  String duration = '00:00:00';
  String get timer => _getDuration();
  bool isRun = false;

  ParserClass({required this.project, required this.parserArr});

  String _getDuration() {
    if (isRun == true) {
      Duration val = Duration(seconds: _timer.tick);
      DateTime dur = DateTime.parse('0000-00-00 00:00:00').add(val);
      duration = DateFormat('HH:mm:ss').format(dur);
    }
    return duration;
  }

  Future<String?> start() async {
    isRun = true;
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      pageParserKey.currentState!.update();
    });
    String? res;
    // подгрузить git
    res = await gitUpdate();
    if (res != null) {
      isRun = false;
      _timer.cancel();
      return res;
    }
    // проверка актуальных для сканирования ссылок через lcp
    res = await lcpUpdate();
    if (res != null) {
      isRun = false;
      _timer.cancel();
      return res;
    }
    // получить ссылки из sitemap.xml
    res = await sitemapUpdate();
    if (res != null) {
      isRun = false;
      _timer.cancel();
      return res;
    }
    // проверка наличия html файлов
    res = await htmlUpdate();
    if (res != null) {
      isRun = false;
      _timer.cancel();
      return res;
    }
    // сканирование
    res = await scan();
    if (res != null) {
      isRun = false;
      _timer.cancel();
      return res;
    }
    return null;
  }

  Future<String?> gitUpdate() async {
    if (project.isGitHub == false) {
      return null;
    }
    if (project.gitHubRepository.trim().isEmpty) {
      return 'Укажите git-репозиторий!';
    }
    if (project.isSaveHtml == false) {
      return 'Необходимо включить сохранение html файлов!';
    }
    if (project.saveHtmlPath.trim().isEmpty) {
      return 'Укажите директорию для сохранения html файлов!';
    }

    // Синхронизация github
    print('Старт github!!!');
    print('out: ' + project.gitHubRepository);
    print('to: ' + project.saveHtmlPath);

    // создание папки, если не существует
    Directory htmlDir = Directory(project.saveHtmlPath);
    if ((await htmlDir.exists()) == false) {
      await htmlDir.create(recursive: true);
    }

    // git clone
    var shell = Shell();
    List<ProcessResult> res = await shell
        .run('git clone ${project.gitHubRepository} ${project.saveHtmlPath}');
    print(res);
  }

  Future<String?> lcpUpdate() async {}

  Future<String?> sitemapUpdate() async {
    if (project.isSitemap == false) {
      return null;
    }
    if (project.sitemapPath.trim().isEmpty) {
      return 'Укажите директорию к sitemap.xml!';
    }

    // добавление ссылок sitemap в бд
    print('Старт sitemap!!!');
    Map<String, dynamic> sitemapArr =
        await lcp.filegetcontents(project.sitemapPath);
    if (sitemapArr['error'] == false) {
      return sitemapArr['string'];
    }
    XmlDocument document = XmlDocument.parse(sitemapArr['string']);
    final urlArr = document.findAllElements('url');
    for (var item in urlArr) {
      String cat = lcp.getcuts(
          item.getElement('loc')!.text, project.http, project.domenSite);
      String? daterEditStr = item.getElement('lastmod')?.text;
      DateTime daterEdit =
          daterEditStr == null ? DateTime.now() : DateTime.parse(daterEditStr);
      String id =
          await lcp.getcelldb('id', 'id', 'parser', 'cat=\'$cat\'', '', '1');
      if (id.isNotEmpty) {
        String daterEditTbl = await lcp.getcelldb(
            'dater_edit', 'dater_edit', 'parser', 'id=\'$id\'', '', '1');
        if (DateTime.parse(daterEditTbl) == daterEdit) {
          continue;
        }
        for (int i = 0; i < parserArr.length; i++) {
          if (parserArr[i].id.toString() == id) {
            Parser parserItem = parserArr[i];
            parserItem.idStatus = 1;
            parserItem.daterEdit = daterEdit;
            Map<String, dynamic> res = await lcp.updatevaluedb(Parser.table,
                parserItem.toMap(), 'id=\'' + parserItem.id.toString() + '\'');
            if (res['error'] == false) {
              return 'Sitemap. Не удалось обновить статус ссылки: ' +
                  parserItem.cat;
            }
            parserArr[i] = parserItem;
            break;
          }
        }
        continue;
      }
      Parser parserItem = Parser(
        cat: cat,
        daterEdit: daterEdit,
        idProject: project.id ?? 0,
      );
      Map<String, dynamic> res =
          await lcp.addvaluedb(Parser.table, parserItem.toMap());
      if (res['error'] == false) {
        return 'Sitemap. Не удалось сохранить ссылку: ' + parserItem.cat;
      }
      parserArr.add(parserItem);
    }
    pageParserKey.currentState!.update();
    return null;
  }

  Future<String?> htmlUpdate() async {
    if (project.isSaveHtml == false) {
      return null;
    }
    if (project.saveHtmlPath.trim().isEmpty) {
      return 'Укажите директорию для сохранения html файлов!';
    }

    // Проверка наличия html файлов
    print('Старт html!!!');
    for (var i = 0; i < parserArr.length; i++) {
      String path = parserArr[i].html ?? '';
      if ((path == '') || (path == 'null')) {
        parserArr[i].isHtml = false;
        parserArr[i].html = '';
      } else {
        File file = File(path);
        if ((await file.exists()) == false) {
          parserArr[i].isHtml = false;
          parserArr[i].html = '';
        } else {
          parserArr[i].isHtml = true;
        }
      }
    }
  }

  Future<String?> scan() async {
    // добавление главной страницы
    String res = '';
    res = await lcp.getcelldb(
        'id',
        'id',
        Parser.table,
        'id_project=\'' +
            project.id.toString() +
            '\' AND cat=\'' +
            project.url +
            '\'',
        '',
        '1');
    if (res.isEmpty) {
      Parser mainItem = Parser(
        cat: project.url,
        daterEdit: DateTime.now(),
        idProject: project.id ?? 0,
      );
      Map<String, String> res =
          await lcp.addvaluedb(Parser.table, mainItem.toMap());
      if (res['error'] == false) {
        return 'Ошибка при добавлении главной страницы!';
      }
      await pageParserKey.currentState!.refresh();
    }

    // сканирование
    print('Старт scan!!!');
    ReceivePort receivePort = ReceivePort();
    Isolate isolate = await Isolate.spawn(parserStart, {
      'sendPort': receivePort.sendPort,
      'parserClass': this,
    });

    receivePort.listen((dynamic receivedData) async {
      stdout.writeln('ѕолучены новые данные из нового изол€та: $receivedData');

// в случае, если получили сообщение
      if (receivedData['action'] == 'update_status') {
        parserArr[receivedData['i']].idStatus = receivedData['idStatus'];
        parserArr[receivedData['i']].comment = receivedData['comment'];
        Map<String, String> res = await lcp.updatevaluedb(
            Parser.table,
            parserArr[receivedData['i']].toMap(),
            'id=\'' + parserArr[receivedData['i']].id.toString() + '\'');
        if (res['error'] == false) {
          print('Ошибка при сохранении "' +
              parserArr[receivedData['i']].cat +
              '"!');
        }
        pageParserKey.currentState!.update();
      } else if (receivedData['action'] == 'addvaluedb') {
        List<String> urlArr = receivedData['urlArr'];
        for (String url in urlArr) {
          Parser newItem = Parser(
            cat: url,
            daterEdit: DateTime.now(),
            idProject: project.id ?? 0,
          );
          Map<String, String> res =
              await lcp.addvaluedb(Parser.table, newItem.toMap());
          if (res['error'] == null) {
            res = await lcp.savedb(
                Parser.table, newItem.toMap(), 'cat=\'' + newItem.cat + '\'');
          }
          if (res['error'] == 'false') {
            print('Ошибка при сохранении "' + url + '"!');
          } else {
            print('id:');
            print(res);
            print(newItem.toMap());
            newItem.id = int.parse(res['id'].toString().trim().isEmpty
                ? '0'
                : res['id'].toString());
            parserArr.add(newItem);
          }
        }
        pageParserKey.currentState!.update();
      } else if (receivedData['action'] == 'end') {
        isolate.kill(priority: Isolate.immediate);
        receivePort.close();
        isRun = false;
        _timer.cancel();
        print('Сканирование завершено!');
        pageParserKey.currentState!.update();
      }
    });

    return null;
  }

  List<String> searchNewUrl({required String data, String search = 'href'}) {
    List<String> res = [];
    int pos = 0;
    int posStart1 = 0;
    int posEnd1 = 0;
    int posStart2 = 0;
    int posEnd2 = 0;
    int k = 0;

    List<String> hrefArr = lcp.explode(search, data);
    if (hrefArr.length <= 1) {
      return res;
    }
    hrefArr.removeAt(0);
    for (String html in hrefArr) {
      posStart1 = html.indexOf('"', 0) + 1;
      posEnd1 = html.indexOf('"', posStart1);
      posStart2 = html.indexOf('\'', 0) + 1;
      posEnd2 = html.indexOf('\'', posStart2);
      bool var1 = true;
      bool var2 = true;
      if (((posStart1 == -1) || (posEnd1 == -1)) ||
          ((posStart1 == 0) || (posEnd1 == 0))) {
        var1 = false;
      }
      if (((posStart2 == -1) || (posEnd2 == -1)) ||
          ((posStart2 == 0) || (posEnd2 == 0))) {
        var2 = false;
      }
      if ((var1 == true) && (var2 == true)) {
        if (posStart1 < posStart2) {
          res.add(html.substring(posStart1, posEnd1));
          // print(html.substring(posStart1, posEnd1));
        } else {
          res.add(html.substring(posStart2, posEnd2));
          // print(html.substring(posStart2, posEnd2));
        }
      } else if (var1 == true) {
        res.add(html.substring(posStart1, posEnd1));
        // print(html.substring(posStart1, posEnd1));
      } else if (var2 == true) {
        res.add(html.substring(posStart2, posEnd2));
        // print(html.substring(posStart2, posEnd2));
      }
    }
    return res;
  }

  bool isExternalLink(url) {
    // если это внешняя ссылка - true
    String link = lcp.getcuts(url, project.http, project.domenSite);
    if (link.contains(project.http + '://' + project.domenSite)) {
      return false;
    } else {
      return true;
    }
  }

  String checkLink(String url) {
    // предварительная проверка, каким типом является ссылка
    // Н-р, link, photo, video, music, document и пр.
    List<String> photo = [
      'jpg',
      'jpeg',
      'bmp',
      'png',
      'tiff',
      'gif',
      'svg',
      'ico',
    ];
    List<String> video = [
      'avi',
      'mov',
      'flv',
      'wmv',
      'mp4',
      'mpeg',
      'mkv',
      '3gp',
    ];
    List<String> music = ['mp3', 'wma', 'wav', 'ogg'];
    List<String> document = [
      'zip',
      'rar',
      'doc',
      'docx',
      'xls',
      'xlsx',
      'css',
      'js',
      'xml',
    ];
    String type = url;
    if (type.contains('?')) {
      type = lcp.explode('?', type).first;
    }
    if (type.contains('.')) {
      type = lcp.explode('.', type).last;
    }

    if (photo.contains(type) == true) {
      return 'photo';
    } else if (video.contains(type) == true) {
      return 'video';
    } else if (music.contains(type) == true) {
      return 'music';
    } else if (document.contains(type) == true) {
      return 'document';
    } else {
      return 'link';
    }
  }
}

void parserStart(Map<String, dynamic> param) async {
  SendPort sendPort = param['sendPort'];
  ParserClass parserClass = param['parserClass'];
  List urlAllArr = [];
  for (var i = 0; i < parserClass.parserArr.length; i++) {
// сканировать только со статусом 1 и 2. закомментировано, так как идентификатор происходит по индексу
    // if ((parserClass.parserArr[i].idStatus == 1) ||
    //     (parserClass.parserArr[i].idStatus == 2)) {
    urlAllArr.add(parserClass.parserArr[i].cat);
    // }
  }
  for (var i = 0; i < urlAllArr.length; i++) {
    print('length: ' + urlAllArr.length.toString() + ' | i=' + i.toString());
    sendPort.send({
      'action': 'update_status',
      'i': i,
      'idStatus': 2,
    });

    // проверяем, является ли ссылка ссылкой или это документ или фото...
    if (parserClass.checkLink(urlAllArr[i]) != 'link') {
      sendPort.send({
        'action': 'update_status',
        'i': i,
        'idStatus': 5,
      });
      sleep(Duration(milliseconds: 100));
      continue;
    }

    Uint8List? bytes = await lcp.fileReadAsByte(urlAllArr[i]);

    if (bytes == null) {
      sendPort.send({
        'action': 'update_status',
        'i': i,
        'idStatus': 4,
      });
      sleep(Duration(milliseconds: 100));
      continue;
    }

    Directory dirPath = Directory(parserClass.project.saveHtmlPath +
        '/' +
        urlAllArr[i].toString().replaceAll(parserClass.project.url, ''));
    if ((await dirPath.exists()) == false) {
      await dirPath.create(recursive: true);
    }
    String htmlPath = dirPath.path + 'index.html';
    File fileHtml = await File(htmlPath).writeAsBytes(bytes);
    String resultParserData = await fileHtml.readAsString();

    // если нужно, сохраняем html файл
    if (parserClass.project.isSaveHtml == true) {
      try {
        if (parserClass.checkLink(urlAllArr[i]) == 'link') {
          if (fileHtml.existsSync() == false) {
            // если файл не существует, то создать
            await fileHtml.create(recursive: true);
          }
          await fileHtml.writeAsString(resultParserData);
        }
      } catch (e) {
        // ошибка при сохранении файлов
        sendPort.send({
          'action': 'update_status',
          'i': i,
          'idStatus': 4,
          'comment': 'Ошибка при сохранении файлов!',
        });
        sleep(Duration(milliseconds: 100));
        continue;
      }
    } else {
      await fileHtml.delete();
    }

    // если нужно, ищем в файле новые ссылки
    if (parserClass.project.isSpider == true) {
      List<String> urlArr = [];
      List<String> urlArrHref =
          parserClass.searchNewUrl(data: resultParserData, search: 'href');
      List<String> urlArrSrc =
          parserClass.searchNewUrl(data: resultParserData, search: 'src');
      urlArr.addAll(urlArrHref);
      urlArr.addAll(urlArrSrc);
      List<String> urlArr2 = [];
      for (String url in urlArr) {
        url = lcp.htmlspecialchars(url);
        if (parserClass.checkLink(url) == 'link') {
          url = lcp.getcuts(
              url, parserClass.project.http, parserClass.project.domenSite);
        } else {
          url = lcp.getcuts(
              url, parserClass.project.http, parserClass.project.domenSite);
          if (url.substring(url.length - 1) == '/') {
            url = url.substring(0, url.length - 1);
          }
        }
        bool b = false;
        for (var i2 = 0; i2 < urlAllArr.length; i2++) {
          if (urlAllArr[i2] == url) {
            b = true;
            break;
          }
        }
        if (b == false) {
          // если нельзя добавлять внешние ссылки - пропустить
          if ((parserClass.project.isExternal == false) &&
              (parserClass.isExternalLink(url))) {
            continue;
          }
          // end если нельзя добавлять внешние ссылки - пропустить

          urlArr2.add(url);
          urlAllArr.add(url);
        }
      }
      // print('Новые ссылки:');
      // for (String url in urlArr2) {
      //   print(url);
      // }
      // sendPort.send({
      //   'action': 'end',
      // });
      // return null;
      sendPort.send({
        'action': 'addvaluedb',
        'urlArr': urlArr2,
      });
    }
    // // parserData
    // this.parserDBArr![i].idStatus = parserURL.idStatus;
    sendPort.send({
      'action': 'update_status',
      'i': i,
      'idStatus': 3,
    });
    sleep(Duration(milliseconds: 100));
  }
  sendPort.send({
    'action': 'end',
  });
}
