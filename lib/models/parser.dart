import 'dart:io';
import 'model.dart';
import '../modules/connect_db.dart';

class Parser extends Model {
  static String table = 'parser';
  static List<String> keys = [
    'id',
    'cat',
    'dater_edit',
    'id_project',
    'id_status',
    'comment',
    'id_lcp',
  ];

  int? id;
  String cat;
  DateTime daterEdit;
  int idProject;
  int idStatus;
  String? comment;
  String? idLCP;
  String? html;
  bool isHtml = false;

  Parser({
    this.id,
    required this.cat,
    required this.daterEdit,
    required this.idProject,
    this.idStatus = 1,
    this.comment,
    this.idLCP,
    this.html,
  }) {
    checkHTML();
  }

  Future<bool> checkHTML() async {
    if (html == null) {
      isHtml = false;
      return false;
    }
    if (html.toString().trim().isEmpty) {
      isHtml = false;
      return false;
    }
    File htmlFile = File(html.toString());
    if ((await htmlFile.exists()) == false) {
      isHtml = false;
      return false;
    }
    isHtml = true;
    return true;
  }

  Map<String, String> toMap() {
    Map<String, String> map = {
      'cat': cat,
      'dater_edit': daterEdit.toString(),
      'id_project': idProject.toString(),
      'id_status': idStatus.toString(),
    };

    if (id != null) {
      map['id'] = id.toString();
    }
    if (comment != null) {
      map['comment'] = comment.toString();
    }
    if (idLCP != null) {
      map['id_lcp'] = idLCP.toString();
    }
    return map;
  }

  static Parser fromMap(Map<String, dynamic> map) {
    return Parser(
      id: int.parse(map['id']),
      cat: lcp.htmlspecialcharsDecode(map['cat']),
      daterEdit: DateTime.parse(map['dater_edit']),
      idProject: int.parse(map['id_project']),
      idStatus: int.parse(map['id_status']),
      comment: map['comment'],
      idLCP: map['id_lcp'],
    );
  }
}
