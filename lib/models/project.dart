import 'model.dart';

class Project extends Model {
  static String table = 'project';
  static List<String> keys = [
    'id',
    'title',
    'url',
    'get_param',
    'isSpider',
    'isSitemap',
    'Sitemap_path',
    'isGitHub',
    'GitHub_repository',
    'isSaveHTML',
    'SaveHTML_path',
    'isCron',
    'cron',
    'isLCP',
    'LCP_SELECT',
    'disallow',
    'isExternal',
  ];

  int? id;
  String title;
  String url;
  String? getParam;
  bool isSpider;
  bool isSitemap;
  String sitemapPath;
  bool isGitHub;
  String gitHubRepository;
  bool isSaveHtml;
  String saveHtmlPath;
  bool isCron;
  String cron;
  bool isLcp;
  String lcpSelect;
  String disallow;
  bool isExternal;
  late String domenSite;
  late String http;

  Project({
    this.id,
    required this.title,
    required this.url,
    this.getParam,
    this.isSpider = true,
    this.isSitemap = false,
    required this.sitemapPath,
    this.isGitHub = false,
    required this.gitHubRepository,
    this.isSaveHtml = false,
    required this.saveHtmlPath,
    this.isCron = false,
    required this.cron,
    this.isLcp = false,
    required this.lcpSelect,
    required this.disallow,
    this.isExternal = false,
  }) {
    http = this.url;
    domenSite = this.url;
    if (this.id != null) {
      if (http.contains('https')) {
        http = 'https';
      } else {
        http = 'http';
      }
      domenSite = domenSite.replaceAll(http + '://', '');
      if (domenSite.substring(domenSite.length - 1) == '/') {
        domenSite = domenSite.substring(0, domenSite.length - 1);
      }
    }
  }

  Map<String, String> toMap() {
    Map<String, String> map = {
      'title': title,
      'url': url,
      'isSpider': isSpider == true ? '1' : '0',
      'isSitemap': isSitemap == true ? '1' : '0',
      'Sitemap_path': sitemapPath,
      'isGitHub': isGitHub == true ? '1' : '0',
      'GitHub_repository': gitHubRepository,
      'isSaveHTML': isSaveHtml == true ? '1' : '0',
      'SaveHTML_path': saveHtmlPath,
      'isCron': isCron == true ? '1' : '0',
      'cron': cron,
      'isLCP': isLcp == true ? '1' : '0',
      'LCP_SELECT': lcpSelect,
      'disallow': disallow,
      'isExternal': isExternal == true ? '1' : '0',
    };

    if (id != null) {
      map['id'] = id.toString();
    }
    if (getParam != null) {
      map['get_param'] = getParam.toString();
    }
    return map;
  }

  static Project fromMap(Map<String, dynamic> map) {
    return Project(
      id: int.parse(map['id']),
      title: map['title'],
      url: map['url'],
      getParam: map['get_param'],
      isSpider: map['isSpider'] == '1' ? true : false,
      isSitemap: map['isSitemap'] == '1' ? true : false,
      sitemapPath: map['Sitemap_path'],
      isGitHub: map['isGitHub'] == '1' ? true : false,
      gitHubRepository: map['GitHub_repository'],
      isSaveHtml: map['isSaveHTML'] == '1' ? true : false,
      saveHtmlPath: map['SaveHTML_path'],
      isCron: map['isCron'] == '1' ? true : false,
      cron: map['cron'],
      isLcp: map['isLCP'] == '1' ? true : false,
      lcpSelect: map['LCP_SELECT'],
      disallow: map['disallow'],
      isExternal: map['isExternal'] == '1' ? true : false,
    );
  }
}
