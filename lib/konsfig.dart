import 'dart:io';

String split = Platform.isWindows ? '\\' : '/';

// локальная БД
String connectDBPathLocal = 'parser_sites' + split + 'db.sqlite3';

const applicationName = 'Парсер'; // название приложения

// debug
// const md5_app = 'EE:D5:CD:73:AF:8B:BC:00:59:66:AF:82:32:4B:39:74';
// const sha1_app = '5D:DA:B4:12:B6:42:EC:9B:73:49:2F:58:C0:E0:28:DF:AE:7B:13:9A';
// const sha256_app =
//     'FE:A6:DA:14:E1:15:82:DA:5F:A3:D1:5E:D2:4D:50:91:59:70:49:39:0E:59:E6:7A:7C:88:66:68:24:84:C5:D1';
// const base64_app = '';