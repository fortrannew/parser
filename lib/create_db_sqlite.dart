import 'package:sqflite/sqflite.dart';

Future<OnDatabaseCreateFn?> createSQLite(Database db, int version) async {
  print('create tables sqlite');
  // parser
  await db.execute('''
CREATE TABLE parser (
    id         INTEGER       PRIMARY KEY ASC AUTOINCREMENT
                             UNIQUE
                             NOT NULL,
    cat        VARCHAR (255) NOT NULL,
    dater_edit VARCHAR (255) NOT NULL,
    id_project INTEGER       NOT NULL
                             REFERENCES project (id),
    id_status  INTEGER       NOT NULL
                             REFERENCES status (id),
    comment    VARCHAR (255),
    id_lcp     VARCHAR (32) 
);
''');

// project
  await db.execute('''
CREATE TABLE project (
    id                INTEGER       PRIMARY KEY ASC AUTOINCREMENT
                                    UNIQUE
                                    NOT NULL,
    title             VARCHAR (255) NOT NULL,
    url               VARCHAR (255) NOT NULL,
    get_param         VARCHAR (255),
    isSpider          INTEGER       NOT NULL
                                    DEFAULT (1),
    isSitemap         INTEGER       NOT NULL
                                    DEFAULT (0),
    Sitemap_path      VARCHAR (255),
    isGitHub          INTEGER       NOT NULL
                                    DEFAULT (0),
    GitHub_repository VARCHAR (255),
    isSaveHTML        INTEGER       NOT NULL
                                    DEFAULT (0),
    SaveHTML_path     VARCHAR (255),
    isCron            INTEGER       NOT NULL
                                    DEFAULT (0),
    cron              VARCHAR,
    isLCP             INTEGER       NOT NULL
                                    DEFAULT (0),
    LCP_SELECT        TEXT,
    disallow          TEXT,
    isExternal        INTEGER       DEFAULT (0) 
                                    NOT NULL
);
''');

// status
  await db.execute('''
CREATE TABLE status (
    id    INTEGER      PRIMARY KEY ASC AUTOINCREMENT
                       UNIQUE
                       NOT NULL,
    title VARCHAR (20) UNIQUE
                       NOT NULL
);
''');

// --------- insert data ---------
// status
  await db
      .execute('''INSERT INTO status (id, title) VALUES (1, 'В очереди');''');
  await db
      .execute('''INSERT INTO status (id, title) VALUES (2, 'Сканируется');''');
  await db
      .execute('''INSERT INTO status (id, title) VALUES (3, 'Завершено');''');
  await db.execute('''INSERT INTO status (id, title) VALUES (4, 'Ошибка');''');
  await db
      .execute('''INSERT INTO status (id, title) VALUES (5, 'Исключено');''');
}
