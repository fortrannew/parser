import 'package:flutter/material.dart';
import 'package:parser/modules/showMessage.dart';
import '../../modules/connect_db.dart';
import '../../models/project.dart';
import '../../models/parser.dart';
import '../../classes/parser.class.dart';

final pageParserKey = GlobalKey<_PageParserStatefulState>();

class PageParser extends StatelessWidget {
  final Project project;
  const PageParser({Key? key, required this.project}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PageParserStateful(
        key: pageParserKey,
        project: project,
      ),
    );
  }
}

class PageParserStateful extends StatefulWidget {
  final Project project;
  const PageParserStateful({Key? key, required this.project}) : super(key: key);

  @override
  State<PageParserStateful> createState() => _PageParserStatefulState();
}

class _PageParserStatefulState extends State<PageParserStateful> {
  String titleStatus = 'Пожалуйста, подождите...';
  bool loadFirst = false;
  late ParserClass parserClass =
      ParserClass(project: widget.project, parserArr: []);
  Map<String, dynamic> statusArr = {};

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadData();
  }

  Future<void> loadData() async {
    titleStatus = 'Обновление данных из git...';
    await parserClass.gitUpdate();
    titleStatus = 'Загружаем ссылки...';
    statusArr = await lcp
        .getarrayvaluedb(['id', 'title'], 'id,title', 'status', '', 'BY id');
    statusArr['index'] = {};
    for (var i = 0; i < statusArr['id'].length; i++) {
      statusArr['index'][statusArr['id'][i].toString()] = i.toString();
    }
    await refresh();
    if (parserClass.parserArr.length <= 0) {
      titleStatus = 'Запустите сканирование!';
    }
  }

  Future<void> refresh() async {
    parserClass.parserArr = [];
    Map<String, dynamic> datasArr = await lcp.getarrayvaluedb(
        Parser.keys,
        lcp.implode(',', Parser.keys),
        Parser.table,
        'id_project=\'' + widget.project.id.toString() + '\'',
        'BY id,id_status',
        '');
    datasArr['id'] = datasArr['id'] ?? [];
    datasArr.remove('getarrayvaluedb_sql');
    for (var i = 0; i < datasArr['id'].length; i++) {
      parserClass.parserArr
          .add(Parser.fromMap(lcp.mapDynamicToString(datasArr, i)));
    }

    setState(() {
      loadFirst = true;
    });
  }

  void update() {
    setState(() {});
  }

  String getStatus(int idStatus) {
    String index = statusArr['index'][idStatus.toString()];
    return statusArr['title'][int.parse(index)];
  }

  Widget getListView() {
    if (loadFirst == false) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    List<Widget> widgetArr = [];
    if (parserClass.parserArr.length > 0) {
      for (var i = 0; i < parserClass.parserArr.length; i++) {
        widgetArr.add(ListTile(
          onTap: () {
            // Navigator.push(
            //     context,
            //     MaterialPageRoute(
            //         builder: (context) => PageParser(
            //               id: datasArr['id'][i],
            //               title: datasArr['title'][i],
            //             )));
          },
          title: Text(parserClass.parserArr[i].cat),
          trailing:
              // Text(getStatus(parserClass.parserArr[i].idStatus)),
              Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(getStatus(parserClass.parserArr[i].idStatus)),
              Visibility(
                visible: ((parserClass.project.isSaveHtml == true) &&
                    (parserClass.parserArr[i].isHtml == true)),
                child: Icon(
                  Icons.text_snippet_outlined,
                  color: Colors.white,
                  size: 32.0,
                ),
              ),
            ],
          ),
        ));
      }
      return ListView(children: widgetArr);
    } else {
      return Center(
        child: Text(titleStatus),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.project.title),
        backgroundColor: Color(0xFF008CFF),
        // leading: titleLeading, // кнопка слева
        // actions: titleActions, // кнопки справа
        // bottom: titleBottom, // контент снизу
        // elevation: this.elevationTitle, // толщина тени (int)
      ),
      body: getListView(),
      // drawer: drawer,
      // backgroundColor: Color(0xFFFFFFFF),
      bottomNavigationBar: Container(
        color: Colors.blue,
        padding: const EdgeInsets.all(10),
        child: Text(
          'Время сканирования: ' +
              parserClass.timer +
              (parserClass.isRun == false ? ' (завершено)' : ' (сканируется)'),
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          // print(lcp.getcuts('https://fortran-new.ru/',
          //     parserClass.project.http, parserClass.project.domenSite));
          // print(parserClass.checkLink(
          //     'https://fortran-new.ru/themes/styles.mon.css?ver=dfgfghfgh'));
          // print(parserClass.isExternalLink('https://vk.com'));
          // Map<String, dynamic> tyt = await lcp.filegetcontents(
          //     'https://fortran-new.ru/novosti/translyaciya-sozdanie-saytov-na-wordpress/');
          // List<String> res2 = parserClass.searchNewUrl(tyt['string']);
          // print(res2);
          // return;
          String? res = await parserClass.start();
          if (res != null) {
            await showMessageError(
                context: context, title: 'Ошибка!', message: res);
          }
          // Navigator.push(context,
          // MaterialPageRoute(builder: (context) => PageSaveProject()));
        },
        tooltip: 'Начать сканирование',
        child:
            Icon(parserClass.isRun == false ? Icons.play_arrow : Icons.pause),
      ),
    );
  }
}
