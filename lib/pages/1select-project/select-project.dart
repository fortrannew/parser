import 'package:flutter/material.dart';
import 'save-project.dart';
import '../../modules/connect_db.dart';
import '../../models/project.dart';
import '../2parser/parser.dart';

class PageSelectProject extends StatefulWidget {
  const PageSelectProject({Key? key}) : super(key: key);

  @override
  State<PageSelectProject> createState() => _PageSelectProjectState();
}

class _PageSelectProjectState extends State<PageSelectProject> {
  List<Project>? projectArr;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    refresh();
  }

  Future<void> refresh() async {
    projectArr = [];
    Map<String, dynamic> datasArr = await lcp.getarrayvaluedb(Project.keys,
        lcp.implode(',', Project.keys), 'project', '', 'BY id', '');
    datasArr['id'] = datasArr['id'] ?? [];

    for (var i = 0; i < datasArr['id'].length; i++) {
      projectArr!.add(Project.fromMap(lcp.mapDynamicToString(datasArr, i)));
    }
    setState(() {});
  }

  Widget getListView() {
    if (projectArr == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    List<Widget> widgetArr = [];
    if (projectArr!.length > 0) {
      for (var i = 0; i < projectArr!.length; i++) {
        widgetArr.add(ListTile(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PageParser(
                          project: projectArr![i],
                        )));
          },
          title: Text(projectArr![i].title),
          trailing: IconButton(
              icon: const Icon(Icons.settings),
              onPressed: () async {
                final res = await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PageSaveProject(
                              project: projectArr![i],
                            )));
                if (res == null) {
                  return;
                }
                this.refresh();
              }),
        ));
      }
      return ListView(children: widgetArr);
    } else {
      return Center(
        child: Text('Создайте новый проект!'),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Выберите проект'),
        backgroundColor: Color(0xFF008CFF),
        // leading: titleLeading, // кнопка слева
        // actions: titleActions, // кнопки справа
        // bottom: titleBottom, // контент снизу
        // elevation: this.elevationTitle, // толщина тени (int)
      ),
      body: getListView(),
      // drawer: drawer,
      // backgroundColor: Color(0xFFFFFFFF),
      // bottomNavigationBar: Container(),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final res = await Navigator.push(context,
              MaterialPageRoute(builder: (context) => PageSaveProject()));
          if (res == null) {
            return;
          }
          this.refresh();
        },
        tooltip: 'Добавить новый проект',
        child: const Icon(Icons.add),
      ),
    );
  }
}
