import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import '../../modules/connect_db.dart';
import '../../modules/showMessage.dart';
import '../../models/project.dart';

class PageSaveProject extends StatefulWidget {
  Project? project;
  PageSaveProject({Key? key, this.project}) : super(key: key);

  @override
  State<PageSaveProject> createState() => _PageSaveProjectState();
}

class _PageSaveProjectState extends State<PageSaveProject> {
  late Project project = widget.project ??
      Project(
        title: '',
        url: '',
        sitemapPath: '',
        gitHubRepository: '',
        saveHtmlPath: '',
        cron: '',
        lcpSelect: '',
        disallow: '',
      );

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(project.isSaveHtml);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(project.id == null ? 'Создать проект' : 'Изменить'),
        backgroundColor: Color(0xFF008CFF),
        // leading: titleLeading, // кнопка слева
        actions: [
          IconButton(
            icon: const Icon(
              Icons.check,
              size: 24,
            ),
            onPressed: () async {
              Map<String, String> res = await lcp.savedb(
                  Project.table,
                  project.toMap(),
                  'id=\'' + (project.id ?? 0).toString() + '\'');
              if (res['error'] == false) {
                print(res);
                showMessageError(
                    context: context,
                    title: 'Ошибка!',
                    message: 'Не удалось сохранить проект!');
                return;
              }
              Navigator.pop(context, true);
            },
            // tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
          ),
        ], // кнопки справа
        // bottom: titleBottom, // контент снизу
        // elevation: this.elevationTitle, // толщина тени (int)
      ),
      body: ListView(children: <Widget>[
        Padding(padding: const EdgeInsets.symmetric(vertical: 10)),
        // Пункт название
        ListTile(
          title: new TextFormField(
            initialValue: project.title,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              filled: true,
              fillColor: Color(0xFFEEEEEE),
              border: const OutlineInputBorder(),
              labelText: "Название проекта",
              hintText: "",
            ),
            onChanged: (String value) {
              project.title = value;
            },
          ),
        ),
        Divider(),

        // URL
        ListTile(
          title: new TextFormField(
            initialValue: project.url,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              filled: true,
              fillColor: Color(0xFFEEEEEE),
              border: const OutlineInputBorder(),
              labelText: "URL",
              hintText: "",
            ),
            onChanged: (String value) {
              project.url = value;
            },
          ),
          subtitle: Text(
              'Введите адрес сайта, который нужно спарсить (https://google.ru/)'),
        ),
        Divider(),

        // Дополнительные get-параметры
        ListTile(
          title: new TextFormField(
            initialValue: project.getParam,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              filled: true,
              fillColor: Color(0xFFEEEEEE),
              border: const OutlineInputBorder(),
              labelText: "Дополнительные get-параметры",
              hintText: "",
            ),
            onChanged: (String value) {
              project.getParam = value;
            },
          ),
          subtitle: Text(
              'При сканировании будут добавляться указанные get-параметры.\nНапример: status=1&method=template'),
        ),
        Divider(),

        // Пункт spider
        CheckboxListTile(
          checkColor: Colors.white,
          value: project.isSpider,
          onChanged: (bool? value) {
            setState(() {
              project.isSpider = value!;
            });
          },
          title: Text(
            'Spider',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Text('Ищет новые ссылки по своему алгоритму'),
        ),
        Divider(),

        // Пункт sitemap.xml
        CheckboxListTile(
          checkColor: Colors.white,
          value: project.isSitemap,
          onChanged: (bool? value) {
            setState(() {
              project.isSitemap = value!;
            });
          },
          title: Text(
            'Sitemap',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Text('Ищет новые ссылки из sitemap'),
        ),
        Visibility(
          visible: project.isSitemap,
          child: ListTile(
            title: new TextFormField(
              initialValue: project.sitemapPath,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                filled: true,
                fillColor: Color(0xFFEEEEEE),
                border: const OutlineInputBorder(),
                labelText: "URL к sitemap.xml",
                hintText: "",
              ),
              onChanged: (String value) {
                project.sitemapPath = value;
              },
            ),
          ),
        ),
        Divider(),

        // Пункт github
        CheckboxListTile(
          checkColor: Colors.white,
          value: project.isGitHub,
          onChanged: (bool? value) {
            setState(() {
              project.isGitHub = value!;
              project.isSaveHtml =
                  project.isGitHub == true ? true : project.isSaveHtml;
            });
          },
          title: Text(
            'GitHub',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Text('Связать CMS LCP с хостингом GitHub или GitLab'),
        ),
        Visibility(
          visible: project.isGitHub,
          child: ListTile(
            title: new TextFormField(
              initialValue: project.gitHubRepository,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                filled: true,
                fillColor: Color(0xFFEEEEEE),
                border: const OutlineInputBorder(),
                labelText: "GitHub репозиторий",
                hintText: "",
              ),
              onChanged: (String value) {
                project.gitHubRepository = value;
              },
            ),
          ),
        ),
        Divider(),

        // Пункт save html
        CheckboxListTile(
          checkColor: Colors.white,
          value: project.isSaveHtml,
          onChanged: (bool? value) {
            setState(() {
              project.isSaveHtml = value!;
            });
          },
          title: Text(
            'Сохранять в HTML',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        Visibility(
          visible: project.isSaveHtml,
          child: ListTile(
            title: new TextFormField(
              initialValue: project.saveHtmlPath,
              keyboardType: TextInputType.text,
              readOnly: true,
              decoration: InputDecoration(
                filled: true,
                fillColor: Color(0xFFEEEEEE),
                border: const OutlineInputBorder(),
                labelText: "Директория для сохранения HTML файлов",
                hintText: "",
              ),
            ),
            trailing: ElevatedButton(
                onPressed: () async {
                  String? selectedDirectory =
                      await FilePicker.platform.getDirectoryPath();
                  if (selectedDirectory == null) {
                    return;
                  }
                  setState(() {
                    project.saveHtmlPath = selectedDirectory;
                  });
                },
                child: Text('Обзор...', style: TextStyle(fontSize: 15))),
          ),
        ),
        Divider(),

        // Пункт cron
        CheckboxListTile(
          checkColor: Colors.white,
          value: project.isCron,
          onChanged: (bool? value) {
            setState(() {
              project.isCron = value!;
            });
          },
          title: Text(
            'Повторять сканирование автоматически',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Text('Работать в фоновом режиме'),
        ),
        Visibility(
          visible: project.isCron,
          child: ListTile(
            title: new TextFormField(
              initialValue: project.cron,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                filled: true,
                fillColor: Color(0xFFEEEEEE),
                border: const OutlineInputBorder(),
                labelText: "CRON",
                hintText: "* * * * *",
              ),
              onChanged: (String value) {
                project.cron = value;
              },
            ),
            subtitle: Text(
                '* Можно использовать в виде cron, в следующем порядке: * * * * * (минуты, часы, день, месяц, год)'),
          ),
        ),
        Divider(),

        // Пункт LCP
        CheckboxListTile(
          checkColor: Colors.white,
          value: project.isLcp,
          onChanged: (bool? value) {
            setState(() {
              project.isLcp = value!;
            });
          },
          title: Text(
            'CMS LCP',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Text(
              'Синхронизировать с CMS LCP. Позволит сканировать только измененные адреса, что существенно сократит время на обход сайта'),
        ),
        Visibility(
            visible: project.isLcp,
            child: Column(
              children: [
                ListTile(
                  title: new TextFormField(
                    initialValue: project.lcpSelect,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Color(0xFFEEEEEE),
                      border: const OutlineInputBorder(),
                      labelText: "SQL SELECT ",
                      hintText: "",
                    ),
                    onChanged: (String value) {
                      project.lcpSelect = value;
                    },
                  ),
                  subtitle: Text(
                      '* SQL запрос к БД для получения ссылок. В итоге важно получить следующие поля: id, dater_edit, cat.\nМожно использовать шаблоны (использовать вместе с квадратными скобками): [offset] - смещение'),
                ),
                ListTile(
                  title:
                      Text('Вложите сохраненный файл в корневую папку проекта'),
                  trailing: ElevatedButton(
                      onPressed: () {},
                      child: Text('Сохранить', style: TextStyle(fontSize: 15))),
                ),
              ],
            )),
        Divider(),

        // Disallow links
        Column(
          children: [
            ListTile(
              title: Text(
                'Запретить сканирование ссылок по маске (с новой строки)',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              title: new TextFormField(
                initialValue: project.disallow,
                keyboardType: TextInputType.multiline,
                maxLines: null,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Color(0xFFEEEEEE),
                  border: const OutlineInputBorder(),
                  labelText: "",
                  hintText: "Н-р, *.jpg",
                ),
                onChanged: (String value) {
                  project.disallow = value;
                },
              ),
            ),
          ],
        ),

        // external links
        CheckboxListTile(
          checkColor: Colors.white,
          value: project.isExternal,
          onChanged: (bool? value) {
            setState(() {
              project.isExternal = value!;
            });
          },
          title: Text(
            'Сканировать внешние ссылки',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
      ]),

      // drawer: drawer,
      // backgroundColor: Color(0xFFFFFFFF),
      // bottomNavigationBar: Container(),
    );
  }
}
