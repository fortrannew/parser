import 'package:flutter/material.dart';

Future<String?> showMessageError({
  required BuildContext context,
  String title = '',
  String message = '',
  List<Widget> actions = const [],
}) async {
  if (actions.length <= 0) {
    actions = [
      TextButton(
        onPressed: () => Navigator.pop(context, 'OK'),
        child: const Text('OK'),
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all<Color>(Color(0xFF629E69)),
        ),
      ),
    ];
  }
  return await showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
            title: title.isNotEmpty ? Text(title) : null,
            content: message.isNotEmpty ? Text(message) : null,
            actions: actions,
          ));
}
