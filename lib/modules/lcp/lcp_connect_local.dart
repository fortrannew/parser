// класс для выбора локальной базы данных и работы конкретно с ней

import 'lcp.dart';
import 'lcp_sqlite.dart';
import '../../konsfig.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';

class LcpLocal extends LCP {
  String localdb = 'sqlite'; // синхронизация с локальной бд (sqlite)
  String applicationName = '';

  late LCPSQLite
      sqliteConnect; // если remotedb=='sqlite', то используется это подключение

  LcpLocal({
    String localdb = 'sqlite',
    required OnDatabaseCreateFn? createDB,
  }) {
    this.localdb = localdb;
    this.applicationName = applicationName;

    if (localdb == 'sqlite') {
      this.sqliteConnect = new LCPSQLite(
          dbPath: connectDBPathLocal,
          applicationName: applicationName,
          createDB: createDB);
    }
  }

  Future<Map<String, dynamic>> sqldb(String sql) async {
    if (this.localdb == 'sqlite') {
      return await this.sqliteConnect.sqldb(sql);
    }
    return {};
  }

  Future<void> sqldbQuery(String sql) async {
    if (this.localdb == 'sqlite') {
      await this.sqliteConnect.sqldbQuery(sql);
    }
  }

  Future<String> getcelldb(
      [String dbcell = '',
      String dbSELECT = '*',
      String dbFROM = '',
      String dbWHERE = '',
      String dbORDER = '',
      String dbLIMIT = '1']) async {
    if (this.localdb == 'sqlite') {
      return await this.sqliteConnect.getcelldb(
          dbcell = dbcell,
          dbSELECT = dbSELECT,
          dbFROM = dbFROM,
          dbWHERE = dbWHERE,
          dbORDER = dbORDER,
          dbLIMIT = dbLIMIT);
    }
    return '';
  }

  Future<void> deletedb([
    String dbFROM = '',
    String dbWHERE = '',
  ]) async {
    if (this.localdb == 'sqlite') {
      return await this.sqliteConnect.deletedb(
            dbFROM = dbFROM,
            dbWHERE = dbWHERE,
          );
    }
  }

  Future<Map<String, dynamic>> getarrayvaluedb(
      [List<String>? dbcellarr,
      String dbSELECT = '*',
      String dbFROM = '',
      String dbWHERE = '',
      String dbORDER = '',
      String dbLIMIT = '']) async {
    if (this.localdb == 'sqlite') {
      return await this.sqliteConnect.getarrayvaluedb(
          dbcellarr = dbcellarr,
          dbSELECT = dbSELECT,
          dbFROM = dbFROM,
          dbWHERE = dbWHERE,
          dbORDER = dbORDER,
          dbLIMIT = dbLIMIT);
    }
    return {};
  }

  Future<Map<String, String>> updatevaluedb(
      String dbFROM, Map<String, String> dbPARAM, String dbWHERE) async {
    dbPARAM['dater_edit'] =
        DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
    List<String> dbkeys = dbPARAM.keys.toList();
    List<String> md5Arr = [];
    for (var i = 0; i < dbkeys.length; i++) {
      md5Arr.add(dbPARAM[dbkeys[i].toString()].toString());
    }
    dbPARAM['md5_edit'] = this.md5LCP(this.implode('', md5Arr));

    if (this.localdb == 'sqlite') {
      return await this
          .sqliteConnect
          .updatevaluedb(dbFROM, dbPARAM, dbWHERE, returnId: false);
    }
    return {};
  }

  Future<bool> existDB() async {
    // проверка наличия уже созданной БД в системе
    bool res = false;
    if (this.localdb == 'sqlite') {
      return await this.sqliteConnect.existDB();
    }
    return res;
  }

  Future<List<String>> getListTables() async {
    if (this.localdb == 'sqlite') {
      return await this.sqliteConnect.getListTables();
    }
    return [];
  }

  Future<String> getQueryCreateTable(String table) async {
    if (this.localdb == 'sqlite') {
      return await this.sqliteConnect.getQueryCreateTable(table);
    }
    return '';
  }

  Future<String> getQueryUpdateTableFromRemote(String query, String table,
      {String typeRemoteDB = 'mysql'}) async {
    if (this.localdb == 'sqlite') {
      if (typeRemoteDB == 'mysql') {
        return await this
            .sqliteConnect
            .getQueryUpdateTableFromMysql(query, table);
      }
    }
    return '';
  }

  String getQueryCreateTableFromRemote(String query,
      {String typeRemoteDB = 'mysql'}) {
    if (this.localdb == 'sqlite') {
      if (typeRemoteDB == 'mysql') {
        return this.sqliteConnect.getQueryCreateTableFromMysql(query);
      }
    }
    return '';
  }

  Future<void> deleteTable(String table) async {
    if (this.localdb == 'sqlite') {
      return await this.sqliteConnect.deleteTable(table);
    }
  }

  Future<Map<String, dynamic>> getMapFieldsFromTable(String table) async {
    if (this.localdb == 'sqlite') {
      return await this.sqliteConnect.getMapFieldsFromTable(table);
    }
    return {};
  }

  Future<List<String>> getListFieldsNameFromTable(String table) async {
    if (this.localdb == 'sqlite') {
      return await this.sqliteConnect.getListFieldsNameFromTable(table);
    }
    return [];
  }

  Future<Map<String, String>> addvaluedb(
      String dbFROM, Map<String, String> dbPARAM) async {
    if (dbPARAM['dater_create'] == null) {
      dbPARAM['dater_create'] =
          DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
    }
    if (dbPARAM['dater_edit'] == null) {
      dbPARAM['dater_edit'] =
          DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
    }
    List<String> dbkeys = dbPARAM.keys.toList();
    List<String> md5Arr = [];
    for (var i = 0; i < dbkeys.length; i++) {
      md5Arr.add(dbPARAM[dbkeys[i].toString()].toString());
    }
    dbPARAM['md5_edit'] = this.md5LCP(this.implode('', md5Arr));
    if (dbPARAM['id'] == null) {
      dbPARAM['id'] = dbPARAM['md5_edit'].toString();
    }
    if ((dbPARAM['id'] == '0') ||
        (dbPARAM['id'] == '') ||
        (dbPARAM['id'] == 'null')) {
      dbPARAM['id'] = dbPARAM['md5_edit'].toString();
    }

    if (this.localdb == 'sqlite') {
      return this.sqliteConnect.addvaluedb(dbFROM, dbPARAM, returnId: false);
    }
    return {};
  }

  Future<Map<String, String>> savedb(
      String dbFROM, Map<String, String> dbPARAM, String dbWHERE) async {
    Map<String, String> resArr = {};
    List<String> dbkeys = dbPARAM.keys.toList();
    String res = await this.getcelldb(
        dbkeys[0].toString(), dbkeys[0].toString(), dbFROM, dbWHERE, '', '1');

    if (res.isNotEmpty) {
      dbPARAM['dater_create'] =
          DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
    }
    dbPARAM['dater_edit'] =
        DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
    List<String> md5Arr = [];
    for (var i = 0; i < dbkeys.length; i++) {
      md5Arr.add(dbPARAM[dbkeys[i].toString()].toString());
    }
    dbPARAM['md5_edit'] = this.md5LCP(this.implode('', md5Arr));
    if (dbPARAM['id'] == null) {
      dbPARAM['id'] = dbPARAM['md5_edit'].toString();
    }
    if ((dbPARAM['id'] == '0') ||
        (dbPARAM['id'] == '') ||
        (dbPARAM['id'] == 'null')) {
      dbPARAM['id'] = dbPARAM['md5_edit'].toString();
    }

    if (this.localdb == 'sqlite') {
      resArr = await this.sqliteConnect.savedb(
          dbFROM = dbFROM, dbPARAM = dbPARAM, dbWHERE = dbWHERE,
          returnId: false);
    }
    return resArr;
  }
}
