import 'lcp.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class LCPdb extends LCP {
  String applicationName = '';
  String methodPostDB = '';
  Future<String> get pathINI async => await _getPathINI();
  Future<String> get ini async => await _getIni();
  Future<Map<String, String>> get iniArr async => await _getIniArr();

  LCPdb(applicationName, connectSiteUrl, methodPostDB) {
    this.applicationName = applicationName;
    this.connectSiteUrl = connectSiteUrl;
    this.methodPostDB = methodPostDB;
    this.dbType = methodPostDB;
  }

  Future<String> _getPathINI() async {
    return await this.pathDocuments + '.' + applicationName;
  }

  Future<String> _getIni() async {
    String iniStr = '';
    if (kIsWeb) {
      iniStr = this.cookie!.getCookie('ini');
    } else {
      iniStr = await this.fileRead(await this._getPathINI());
    }
    if (iniStr.isEmpty) {
      return '';
    }
    Map<String, String> iniArr = this.unserialize(iniStr);
    return iniArr['cur'] ?? '';
  }

  Future<Map<String, String>> _getIniArr() async {
    String iniStr = '';
    if (kIsWeb) {
      iniStr = this.cookie!.getCookie('ini');
    } else {
      iniStr = await this.fileRead(await this._getPathINI());
    }
    Map<String, String> iniArr = this.unserialize(iniStr);
    Map<String, String> iniArrRes = this.unserialize(iniArr['ini'].toString());
    return iniArrRes;
  }

  Future<void> saveIni(String data) async {
    Map<String, String> iniArr = await this._getIniArr();
    Map<String, String> iniArrRes = {};
    bool b = false;
    for (var i = 0; i < iniArr.length; i++) {
      if (iniArr[i.toString()] == data) {
        b = true;
        break;
      }
    }
    if (b == false) {
      iniArr[iniArr.length.toString()] = data;
    }
    iniArrRes['cur'] = data;
    iniArrRes['ini'] = this.serialize(iniArr);
    if (kIsWeb) {
      this.cookie!.setCookie('ini', this.serialize(iniArrRes));
    } else {
      await this.fileWrite(await this._getPathINI(), this.serialize(iniArrRes));
    }
  }

  Future<bool> deleteIni(String ini) async {
    Map<String, String> iniArr = await this._getIniArr();
    Map<String, String> iniArrRes = {};
    Map<String, String> iniArrNew = {};
    int k = 0;
    for (var i = 0; i < iniArr.length; i++) {
      if (iniArr[i.toString()] != ini) {
        iniArrNew[k.toString()] = iniArr[i.toString()].toString();
        k++;
      }
    }
    iniArrRes['cur'] = this.md5LCP('-');
    iniArrRes['ini'] = this.serialize(iniArrNew);
    if (kIsWeb) {
      this.cookie!.setCookie('ini', this.serialize(iniArrRes));
    } else {
      await this.fileWrite(await this._getPathINI(), this.serialize(iniArrRes));
    }
    return true;
  }

  // Future<String> sqldb(String sql) async {
  //   return await this.filegetcontents(this.connectSiteUrl, 'post', {
  //     'post': this.methodPostDB,
  //     'function': 'sqldb',
  //     'sql': sql,
  //   });
  // }

  Future<void> sqldbQuery(String sql) async {
    await this.filegetcontents(this.connectSiteUrl, 'post', {
      'post': this.methodPostDB,
      'function': 'sqldb_query',
      'sql': sql,
    });
  }
}
