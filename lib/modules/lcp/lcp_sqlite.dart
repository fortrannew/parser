// *** Установка ***
// Чтобы пакет работал нормально - необходимо в терминале выполнить команды:
// - flutter pub add sqflite_common_ffi
// - flutter pub add sqflite

import 'lcp.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common/sqlite_api.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import 'package:path_provider/path_provider.dart' as provided;
import 'dart:io';

class LCPSQLite extends LCP {
  String applicationName = '';
  Future<String> get pathINI async => await _getPathINI();
  Future<String> get ini async => await _getIni();
  Future<Map<String, String>> get iniArr async => await _getIniArr();
  late Database connected;
  String dbPath = '';
  OnDatabaseCreateFn? createDB;

  LCPSQLite({
    String dbPath = '',
    String applicationName = '',
    required OnDatabaseCreateFn? createDB,
  }) {
    this.applicationName = applicationName;
    this.dbPath = dbPath;
    this.createDB = createDB;
  }

  Future<bool> connect({bool firstRun = false}) async {
    print('connect sqlite');
    String split = Platform.isWindows ? '\\' : '/';
    try {
      Directory dir = await provided.getApplicationDocumentsDirectory();
      List<String> lastDirArr = this.explode(split, this.dbPath);
      String lastDir = '';
      if (lastDirArr.length > 1) {
        lastDir = lastDirArr[lastDirArr.length - 2];
      }
      print(dir.path + split + lastDir);
      Directory path = Directory(dir.path + split + lastDir);
      if (!(await path.exists())) {
        path.create(recursive: true);
      }

      if (Platform.isWindows) {
        sqfliteFfiInit();
        var databaseFactory = databaseFactoryFfi;
        this.connected = await databaseFactory.openDatabase(
            dir.path + split + this.dbPath,
            options: OpenDatabaseOptions(onCreate: createDB, version: 1));
      }
      if ((Platform.isAndroid) || (Platform.isIOS) || (Platform.isMacOS)) {
        this.connected = await openDatabase(dir.path + split + this.dbPath,
            onCreate: createDB, version: 1);
      }
    } catch (e) {
      print('Подключиться к sqlite не удалось!!!');
      return false;
    }

    // получаем список таблиц
    if (firstRun == true) {
      print('получаем список таблиц');
      List<String> tblArr = await this.getListTables();
      if (tblArr.isEmpty) {
        // если ни одной таблицы в бд нет - создать app_setting
        this.createTableAppSetting();
      } else if (tblArr.contains('app_setting') == false) {
        // если служебной таблицы app_setting нет - создать
        this.createTableAppSetting();
      }
    }
    return true;
  }

  Future<bool> disconnect() async {
    try {
      await this.connected.close();
    } catch (e) {
      print('Закрыть соединение sqlite не удалось!!!');
      return false;
    }
    return true;
  }

  Future<bool> existDB() async {
    // проверка наличия уже созданной БД в системе
    if (kIsWeb) {
      return false;
    }
    String split = Platform.isWindows ? '\\' : '/';
    Directory dir = await provided.getApplicationDocumentsDirectory();
    List<String> lastDirArr = this.explode(split, this.dbPath);
    String lastDir = '';
    if (lastDirArr.length > 1) {
      lastDir = lastDirArr[lastDirArr.length - 2];
    }
    print(dir.path + split + lastDir);
    Directory path = Directory(dir.path + split + lastDir);
    if (!(await path.exists())) {
      return false;
    }
    final file = File(dir.path + split + this.dbPath);
    if (!(await file.exists())) {
      return false;
    }
    return true;
  }

  Future<bool> createTableAppSetting() async {
    try {
      await this.sqldbQuery('''
    CREATE TABLE app_setting (
        name VARCHAR(255) PRIMARY KEY
                          UNIQUE
                          NOT NULL,
        value VARCHAR(255)
    );
    INSERT INTO app_setting(name, value) VALUES ('version_structure','0.0.0');
    ''');
    } catch (e) {
      return false;
    }
    return true;
  }

  Future<String> _getPathINI() async {
    return await this.pathDocuments + '.' + applicationName;
  }

  Future<String> _getIni() async {
    String iniStr = '';
    if (kIsWeb) {
      iniStr = this.cookie!.getCookie('ini');
    } else {
      iniStr = await this.fileRead(await this._getPathINI());
    }
    if (iniStr.isEmpty) {
      return '';
    }
    Map<String, String> iniArr = this.unserialize(iniStr);
    return iniArr['cur'] ?? '';
  }

  Future<Map<String, String>> _getIniArr() async {
    String iniStr = '';
    if (kIsWeb) {
      iniStr = this.cookie!.getCookie('ini');
    } else {
      iniStr = await this.fileRead(await this._getPathINI());
    }
    Map<String, String> iniArr = this.unserialize(iniStr);
    Map<String, String> iniArrRes = this.unserialize(iniArr['ini'].toString());
    return iniArrRes;
  }

  Future<void> saveIni(String data) async {
    Map<String, String> iniArr = await this._getIniArr();
    Map<String, String> iniArrRes = {};
    bool b = false;
    for (var i = 0; i < iniArr.length; i++) {
      if (iniArr[i.toString()] == data) {
        b = true;
        break;
      }
    }
    if (b == false) {
      iniArr[iniArr.length.toString()] = data;
    }
    iniArrRes['cur'] = data;
    iniArrRes['ini'] = this.serialize(iniArr);
    if (kIsWeb) {
      this.cookie!.setCookie('ini', this.serialize(iniArrRes));
    } else {
      await this.fileWrite(await this._getPathINI(), this.serialize(iniArrRes));
    }
  }

  Future<bool> deleteIni(String ini) async {
    Map<String, String> iniArr = await this._getIniArr();
    Map<String, String> iniArrRes = {};
    Map<String, String> iniArrNew = {};
    int k = 0;
    for (var i = 0; i < iniArr.length; i++) {
      if (iniArr[i.toString()] != ini) {
        iniArrNew[k.toString()] = iniArr[i.toString()].toString();
        k++;
      }
    }
    iniArrRes['cur'] = this.md5LCP('-');
    iniArrRes['ini'] = this.serialize(iniArrNew);
    if (kIsWeb) {
      this.cookie!.setCookie('ini', this.serialize(iniArrRes));
    } else {
      await this.fileWrite(await this._getPathINI(), this.serialize(iniArrRes));
    }
    return true;
  }

  Future<Map<String, dynamic>> sqldb(String sql) async {
    Map<String, dynamic> resArr = {};
    List<Map<String, Object?>> res;
    bool con = await connect();
    if (con == false) {
      return {};
    }
    try {
      List<String> keys = [];
      res = await this.connected.rawQuery(sql);
      if (res.length <= 0) {
        return {};
      }
      keys = res[0].keys.toList();
      for (var key in keys) {
        resArr[key] = {};
      }
      if (keys.length <= 0) {
        return {};
      }
      int rowCount = 0;
      for (var row in res) {
        for (var key in keys) {
          resArr[key][rowCount] = row[key].toString();
        }
        rowCount++;
      }
    } catch (e) {
      print('Ошибка в запросе: ' + sql);
    }
    await disconnect();
    return resArr;
  }

  Future<void> sqldbQuery(String sql) async {
    List<String> resArr = this.explode(";", sql);
    bool con = await connect();
    if (con == false) {
      return;
    }
    for (var query in resArr) {
      if (query.trim().isEmpty) {
        continue;
      }
      try {
        await this.connected.rawQuery(query + ';');
      } catch (e) {
        print('Ошибка в запросе: ' + query);
      }
    }
    await disconnect();
  }

  Future<Map<String, dynamic>> getarrayvaluedb(
      [List<String>? dbcellarr,
      String dbSELECT = '*',
      String dbFROM = '',
      String dbWHERE = '',
      String dbORDER = '',
      String dbLIMIT = '']) async {
    Map<String, dynamic> param = {};
    String s = this.getarrayvaluedbSQL(
      dbcellarr = dbcellarr,
      dbSELECT = dbSELECT,
      dbFROM = dbFROM,
      dbWHERE = dbWHERE,
      dbORDER = dbORDER,
      dbLIMIT = dbLIMIT,
    );
    if (s == 'false') {
      return {'getarrayvaluedb_sql': s};
    }
    Map<String, dynamic> res = await this.sqldb(s);
    res['getarrayvaluedb_sql'] = s;
    return res;
  }

  Future<Map<String, String>> insertvaluedb(
      String dbFROM, String dbCOLUMNS, String dbVALUES) async {
    String s = this.insertvaluedbSQL(dbFROM, dbCOLUMNS, dbVALUES);
    if (s == 'false') {
      return {
        'error': 'false',
        'id': '',
        'string': 'ERROR: TABLE COLUMN AND VALUE EMPTY'
      };
    }
    bool con = await connect();
    if (con == false) {
      return {};
    }
    int res = await this.connected.rawInsert(s);
    await disconnect();
    if (res == 0) {
      return {'error': 'false', 'id': '', 'string': s};
    }
    return {'error': 'true', 'id': res.toString(), 'string': s};
  }

  Future<Map<String, String>> addvaluedb(
      String dbFROM, Map<String, String> dbPARAM,
      {returnId = true}) async {
    String s = this.addvaluedbSQL(dbFROM, dbPARAM);
    if (s == 'false') {
      return {
        'error': 'false',
        'id': '',
        'string': 'ERROR: TABLE COLUMN AND VALUE EMPTY'
      };
    }
    bool con = await connect();
    if (con == false) {
      return {};
    }
    int res = 0;
    try {
      res = await this.connected.rawInsert(s);
    } catch (e) {
      await disconnect();
      return {'error': 'false', 'id': '', 'string': s};
    }
    await disconnect();
    if (res == 0) {
      return {'error': 'false', 'id': '', 'string': s};
    }
    if (returnId == false) {
      return {'error': 'true', 'id': dbPARAM['id'].toString(), 'string': s};
    }
    return {'error': 'true', 'id': res.toString(), 'string': s};
  }

  Future<Map<String, String>> savedb(
      String dbFROM, Map<String, String> dbPARAM, String dbWHERE,
      {returnId = true}) async {
    List<String> dbPARAM_field = dbPARAM.keys.toList();
    String res = await this.getcelldb(dbPARAM_field[0].toString(),
        dbPARAM_field[0].toString(), dbFROM, dbWHERE, '', '1');
    Map<String, String> resArr = {};
    if (res.isNotEmpty) {
      resArr = await this
          .updatevaluedb(dbFROM, dbPARAM, dbWHERE, returnId: returnId);
      resArr['action'] = 'update';
    } else {
      if (dbPARAM['id'] != null) {
        if (dbPARAM['id']!.isEmpty) {
          dbPARAM.remove('id');
        }
      }
      resArr = await this.addvaluedb(dbFROM, dbPARAM, returnId: returnId);
      resArr['action'] = 'insert';
    }
    return resArr;
  }

  Future<String> getcelldb(
      [String dbcell = '',
      String dbSELECT = '*',
      String dbFROM = '',
      String dbWHERE = '',
      String dbORDER = '',
      String dbLIMIT = '1']) async {
    String s = this.getcelldbSQL(
        dbcell = dbcell,
        dbSELECT = dbSELECT,
        dbFROM = dbFROM,
        dbWHERE = dbWHERE,
        dbORDER = dbORDER,
        dbLIMIT = dbLIMIT);
    Map<String, dynamic> res = await this.sqldb(s);
    List<String> resArr = this.mapToList(res, dbcell);
    if (resArr.length > 0) {
      return resArr[0].toString();
    } else {
      return '';
    }
  }

  Future<void> deletedb([
    String dbFROM = '',
    String dbWHERE = '',
  ]) async {
    Map<String, dynamic> param = {};
    String s = this.deletedbSyncSQL(dbFROM = dbFROM, dbWHERE = dbWHERE);
    if (s == 'false') {
      print('Ошибка при удалении данных! ERROR: TABLE FROM EMPTY');
      return;
    }
    await this.sqldbQuery(s);
  }

  Future<Map<String, String>> updatevaluedb(
      String dbFROM, Map<String, String> dbPARAM, String dbWHERE,
      {returnId = true}) async {
    String s = this.updatevaluedbSQL(dbFROM, dbPARAM, dbWHERE);
    if (s == 'false') {
      return {
        'error': 'false',
        'id': '',
        'string': 'ERROR: TABLE COLUMN AND VALUE EMPTY'
      };
    }
    bool con = await connect();
    if (con == false) {
      return {};
    }
    int res = 0;
    try {
      res = await this.connected.rawUpdate(s);
    } catch (e) {
      await disconnect();
      return {'error': 'false', 'id': '', 'string': s};
    }
    await disconnect();
    if (returnId == false) {
      return {'error': 'true', 'id': dbPARAM['id'].toString(), 'string': s};
    }
    return {'error': 'true', 'id': '', 'string': s};
  }

  Future<List<String>> getListTables() async {
    List<String> resArr = [];
    Map<String, dynamic> resArr2 = await this.sqldb(
        'SELECT name FROM sqlite_master WHERE type=\'table\' AND name<>\'android_metadata\'  AND name<>\'sqlite_sequence\';');
    if (resArr2.isEmpty) {
      return [];
    }
    for (var i = 0; i < resArr2['name'].length; i++) {
      resArr.add(resArr2['name'][i].toString());
    }
    return resArr;
  }

  Future<Map<String, dynamic>> getMapFieldsFromTable(String table) async {
    Map<String, dynamic> resArr = {};
    resArr['name'] = {};
    resArr['type'] = {};
    Map<String, dynamic> resArr2 =
        await this.sqldb('PRAGMA table_info(\'' + table + '\');');
    if (resArr2 != null) {
      if (resArr2['name'] != null) {
        for (var i = 0; i < resArr2['name'].length; i++) {
          String typeField = resArr2['type'][i].toString();
          typeField = typeField.replaceAll('varchar', 'VARCHAR');
          typeField = typeField.replaceAll('datetime', 'DATETIME');
          typeField = typeField.replaceAll('integer', 'INTEGER');
          typeField = typeField.replaceAll('int', 'INTEGER');
          typeField = typeField.replaceAll('boolean', 'BOOLEAN');
          typeField = typeField.replaceAll('bool', 'BOOLEAN');
          typeField = typeField.replaceAll('text', 'TEXT');
          typeField = typeField.replaceAll('blob', 'BLOB');
          resArr['name'][i] = resArr2['name'][i].toString();
          resArr['type'][i] = typeField;
        }
      }
    }

    return resArr;
  }

  Future<List<String>> getListFieldsNameFromTable(String table) async {
    Map<String, dynamic> fieldArr = await this.getMapFieldsFromTable(table);
    List<String> resArr = [];
    for (var i = 0; i < fieldArr['name'].length; i++) {
      resArr.add(fieldArr['name'][i].toString());
    }
    return resArr;
  }

  Future<List<String>> getListFieldsTypeFromTable(String table) async {
    Map<String, dynamic> fieldArr = await this.getMapFieldsFromTable(table);
    List<String> resArr = [];
    for (var i = 0; i < fieldArr['type'].length; i++) {
      resArr.add(fieldArr['type'][i].toString());
    }
    return resArr;
  }

  Future<String> getQueryCreateTable(String table) async {
    String res = '';
    Map<String, dynamic> resFieldArr2 = await this.sqldb(
        'SELECT sql FROM sqlite_master WHERE type=\'table\' AND name=\'' +
            table +
            '\' LIMIT 1;');
    res = resFieldArr2['sql'][0];
    return res;
  }

  String getQueryCreateTableFromMysql(String query) {
    String res = '';
    res = query.replaceAll('`', '');
    res = res.replaceAll('AUTO_INCREMENT', 'AUTOINCREMENT');
    res = res.replaceAll('CHARACTER SET utf8', '');
    res = res.replaceAll('COLLATE utf8_general_ci', '');
    res = res.replaceAll('USING BTREE', '');
    res = explode('ENGINE', res)[0].toString();

    while (res.contains('UNIQUE KEY') == true) {
      String fieldStr = explode('UNIQUE KEY', res)[1].toString();
      String fieldStrReplace = explode('(', fieldStr)[0].toString();
      fieldStr = explode('(', fieldStr)[1].toString();
      fieldStr = explode(')', fieldStr)[0].toString();
      res = res.replaceAll('UNIQUE KEY' + fieldStrReplace, 'UNIQUE');
    }
    return res;
  }

  Future<String> getQueryUpdateTableFromMysql(
      String query, String table) async {
    String queryRemoteCreateTableStr = this.getQueryCreateTableFromMysql(query);
    List<String> fieldLocalArr = await this.getListFieldsNameFromTable(table);

    String localInsertQuery = '';
    localInsertQuery += 'INSERT INTO ' + table + ' (';
    localInsertQuery += this.implode(',', fieldLocalArr);
    localInsertQuery += ') SELECT ';
    localInsertQuery += this.implode(',', fieldLocalArr);
    localInsertQuery += ' FROM sqlitestudio_temp_table;';

    String res = '''PRAGMA foreign_keys = 0;
CREATE TABLE sqlitestudio_temp_table AS SELECT * FROM ${table};
DROP TABLE ${table};
${queryRemoteCreateTableStr};
${localInsertQuery}
DROP TABLE sqlitestudio_temp_table;
PRAGMA foreign_keys = 1;''';
    return res;
  }

  Future<void> deleteTable(String table) async {
    await this.sqldb('DROP TABLE IF EXISTS ' + table + ';');
  }
}
