//
//
// ********** Общий пакет LCP для быстрой разработки приложений **********
//
//
//
//
//
// *** Установка ***
//
// Чтобы пакет работал нормально - необходимо в терминале выполнить команды:
// - flutter pub add crypto
// - flutter pub add http
// - flutter pub add path_provider
// - flutter pub add provider
// - flutter pub add universal_html
//
//
// *** Инициализация ***
// В проекте создайте новый объект:
// final lcp = new LCP();
//
//
//
//
//
//
// *** Как работать с filegetcontents ***
// - Сначала делаем запрос на url адрес страницы. Указываем функцию, которая должна выполниться после завершения парсинга:
// lcp.filegetcontents(
//   'https://svipa.ru/montag/sv.php',
//   filegetcontentresult,
// );
//
// - Затем создайте новую функцию, и пропишите необходимые действия:
// void filegetcontentresult(texts) {
//   print(texts);
// }
//
//
// - Либо используйте более простой синтаксис в одну строку:
// lcp.filegetcontents(
//   'https://svipa.ru/montag/sv.php',
//   (texts){print(texts);}),
// );
//
//
// *** Отображение окна загрузки ***
// setState(() {
//      lcp.loading(loadingShows: true);
//    });
//
//
// *** Отображение ошибки ***
// lcp.message('Ошибка при авторизации!', context);
//
//
//
//

import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:universal_html/html.dart' show document;
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import 'dart:typed_data';
//import 'package:svipa/main.dart';

class LCP extends ChangeNotifier {
  String connectSiteUrl = '';
  String dbType = 'mysqli';
  Future get pathDocuments async => await _getDocuments();
  bool loadingShow = false;
  CookieManager? cookie;
  //Future get pathDownloads async => await _getDownloads();

  LCP({dbType = 'mysqli'}) {
    this.dbType = dbType;
    if (kIsWeb) {
      this.cookie = CookieManager();
    }
  }

  Future<String> _getDocuments() async {
    Directory directory = await getApplicationDocumentsDirectory();
    return directory.path + '/';
  }

  /*Future<String> _getDownloads() async {
    Directory directory = await getExternalStorageDirectory();
    return directory.path;
  }*/

  List<String> explode(String delim, String str) {
    // функция для преобразования строки в массив, с помощью разделителя
    return str.split(delim);
  }

  String implode(String delim, List<String> arr) {
    // функция для преобразования массива в строку, добавляя свой разделитель
    String res = '';
    int arrCount = arr.length;
    for (int i = 0; i < arrCount; i++) {
      res += (i == (arrCount - 1)) ? arr[i] : arr[i] + delim;
    }
    return res;
  }

  Future<Map<String, dynamic>> filegetcontents(
    String filename, [
    String method = 'get',
    Map<String, String>? postArr,
  ]) async {
    Map<String, dynamic> res = {'error': 'false', 'string': '', 'code': '0'};
    final client = http.Client();
    late http.Response response;
    try {
      // получаем контент
      response = (method == 'post')
          ? await client
              .post(Uri.parse(filename), body: postArr)
              .timeout(Duration(seconds: 10))
          : await client
              .get(Uri.parse(filename))
              .timeout(Duration(seconds: 10));
      client.close();
    } catch (e) {
      client.close();
      // проверка интернета
      try {
        response = await client
            .get(Uri.parse('https://ya.ru/'))
            .timeout(Duration(seconds: 10));
        client.close();
      } catch (e) {
        client.close();
        res = {
          'error': 'false',
          'string': 'Нет доступа к Интернету!',
          'code': '555',
        };
        return res;
      }
      try {
        // получаем контент повторно
        response = (method == 'post')
            ? await client
                .post(Uri.parse(filename), body: postArr)
                .timeout(Duration(seconds: 10))
            : await client
                .get(Uri.parse(filename))
                .timeout(Duration(seconds: 10));
        client.close();
      } catch (e) {
        client.close();
        res = {
          'error': 'false',
          'string': 'Не удалось получить контент!',
          'code': '500',
        };
        return res;
      }
    }

    //print(response.statusCode);
    if (response.statusCode == 200) {
      res = {
        'error': 'true',
        'string': response.body,
        'head': response.headers,
        'isRedirect': response.isRedirect,
        'contentLength': response.contentLength,
        'code': '200',
      };
    } else {
      res = {
        'error': 'false',
        'string': 'Запрос вернул ошибку ' + response.statusCode.toString(),
        'code': response.statusCode.toString(),
      };
    }
    return res;
  }

  Future<Uint8List?> fileReadAsByte(String filename) async {
    final client = http.Client();
    late Uint8List res;
    try {
      res = await client.readBytes(Uri.parse(filename));
    } catch (e) {
      return null;
    }
    return res;
  }

  Future<bool> fileWriteFromByte(String filename, Uint8List data) async {
    bool res = false;
    await File(filename).writeAsBytes(data);
    res = true;
    return res;
  }

  String serialize(Map<String, String> arr) {
    String res = '';
    try {
      // получить ключи
      List<String> serializeKeysArr = arr.keys.toList();
      // получить номер сериализации
      int serializeNum = 1;
      for (int i = 0; i < serializeKeysArr.length; i++) {
        List<String> serializeArr =
            explode('[{//serialize//}]', arr[serializeKeysArr[i]].toString());
        if (serializeArr.length > 1) {
          serializeNum = int.parse(serializeArr[0]);
          serializeNum++;
        }
      }
      // преобразовать ключи
      String serializeKeys = implode(
          '[{//key' + serializeNum.toString() + '//}]', serializeKeysArr);
      serializeKeys = htmlspecialchars(serializeKeys);
      // преобразовать значения
      String serializeValues = implode(
          '[{//val' + serializeNum.toString() + '//}]', arr.values.toList());
      serializeValues = htmlspecialchars(serializeValues);
      res = serializeNum.toString() +
          '[{//serialize//}]' +
          serializeKeys +
          '[{//delimer' +
          serializeNum.toString() +
          '}]' +
          serializeValues;
    } catch (e) {
      return '';
    }
    return res;
  }

  Map<String, String> unserialize(String str) {
    // функция для преобразования строки в ассоциативный массив (список)
    Map<String, String> res = {};
    try {
      // получить номер сериализации и финальную строку
      List<String> serializeArr = explode("[{//serialize//}]", str);
      String serializeNum = serializeArr[0];
      str = '';
      for (int i = 1; i < serializeArr.length; i++) {
        str += ((i > 1) ? "[{//serialize//}]" : "") + serializeArr[i];
      }
      // Получить готовый массив
      String resKeysStr = '';
      String resValsStr = '';
      List<String> resKeysStrArr =
          explode("[{//delimer" + serializeNum + "}]", str);
      resKeysStr = resKeysStrArr[0];
      resValsStr = resKeysStrArr[1];
      List<String> resKeys =
          explode("[{//key" + serializeNum + "//}]", resKeysStr);
      List<String> resVals =
          explode("[{//val" + serializeNum + "//}]", resValsStr);
      for (int i = 0; i < resKeys.length; i++) {
        res[resKeys[i]] = htmlspecialcharsDecode(resVals[i]);
      }
    } catch (e) {
      return res;
    }
    return res;
  }

  String htmlspecialchars(String str) {
    // функция, которая преобразовывает специальные символы в HTML сущности
    str = str.replaceAll("&", "&amp;");
    str = str.replaceAll("\"", "&quot;");
    str = str.replaceAll("'", "&#039;");
    str = str.replaceAll("<", "&lt;");
    str = str.replaceAll(">", "&gt;");
    str = str.replaceAll("\\", "&#92;");
    return str;
  }

  String htmlspecialcharsDecode(String str) {
    // функция, которая преобразовывает HTML сущности обратно в специальные символы
    str = str.replaceAll("&amp;", "&");
    str = str.replaceAll("&quot;", "\"");
    str = str.replaceAll("&#039;", "'");
    str = str.replaceAll("&lt;", "<");
    str = str.replaceAll("&gt;", ">");
    str = str.replaceAll("&#92;", "\\");
    return str;
  }

  Future<void> fileWrite(String path, String data) async {
    final File file = File(path);
    await file.writeAsString(data);
  }

  Future<String> fileRead(String path) async {
    String text = '';
    try {
      final File file = File(path);
      text = await file.readAsString();
    } catch (e) {
      print('ERROR LCP fileRead: Couldn\'t read file path ' + path);
    }
    return text;
  }

  String md5LCP(String str) {
    return md5.convert(utf8.encode(str)).toString();
  }

  // Future<int> getcountdb(String dbFROM, [String dbWHERE = '']) async {
  //   String dbFromOne = dbFROM;
  //   List<String> dbFromOneArr = this.explode(',', dbFromOne);
  //   dbFromOne = dbFromOneArr[0];
  //   dbFromOneArr = explode('LEFT', dbFromOne);
  //   dbFromOne = dbFromOneArr[0];
  //   dbFromOne = dbFromOne.trim();
  //   String dbCountStr = '';

  //   String s = '';
  //   if (dbType == 'postgresql') {
  //     dbFromOne = dbFromOne.replaceAll('`', '');
  //     dbCountStr = 'COUNT("' + dbFromOne + '"."id")';
  //     s = 'SELECT ' + dbCountStr + ' ';
  //   } else if ((dbType == 'mysql') || (dbType == 'mysqli')) {
  //     dbFromOne = dbFromOne.replaceAll('`', '');
  //     dbCountStr = 'COUNT(`' + dbFromOne + '`.`id`)';
  //     s = 'SELECT ' + dbCountStr + ' ';
  //   }
  //   if (dbFROM.isNotEmpty) {
  //     s += ' FROM ' + dbFROM + ' ';
  //   } else {
  //     return 0;
  //   }
  //   if (dbWHERE.isNotEmpty) {
  //     s += ' WHERE ' + dbWHERE + ' ';
  //   }
  //   String res = await this.sqldb(s);
  //   if (res.isEmpty) {
  //     return 0;
  //   }
  //   Map<String, String> resArr = unserialize(res);
  //   if (resArr.length <= 0) {
  //     return 0;
  //   }
  //   res = resArr[dbCountStr].toString();
  //   if (res.isEmpty) {
  //     return 0;
  //   }
  //   resArr = unserialize(res);
  //   if (resArr.length <= 0) {
  //     return 0;
  //   }
  //   res = resArr['0'].toString();
  //   return int.parse(res);
  // }

  String getcelldbSQL(
      [String dbcell = '',
      String dbSELECT = '*',
      String dbFROM = '',
      String dbWHERE = '',
      String dbORDER = '',
      String dbLIMIT = '']) {
    String s = '';
    if (dbSELECT.isNotEmpty) {
      s += ' SELECT ' + dbSELECT + ' ';
    } else {
      s += ' SELECT * ';
    }
    if (dbFROM.isNotEmpty) {
      s += ' FROM ' + dbFROM + ' ';
    } else {
      return 'ERROR: TABLE FROM EMPTY';
    }
    if (dbWHERE.isNotEmpty) {
      s += ' WHERE ' + dbWHERE + ' ';
    }
    if (dbORDER.isNotEmpty) {
      s += ' ORDER ' + dbORDER + ' ';
    }
    if (dbLIMIT.isNotEmpty) {
      s += ' LIMIT ' + dbLIMIT + ' ';
    }
    return s;
  }

  Future<String?> getcelldb(
      [String dbcell = '',
      String dbSELECT = '*',
      String dbFROM = '',
      String dbWHERE = '',
      String dbORDER = '',
      String dbLIMIT = '']) async {
    String s = this.getcelldbSQL(
        dbcell = dbcell,
        dbSELECT = dbSELECT,
        dbFROM = dbFROM,
        dbWHERE = dbWHERE,
        dbORDER = dbORDER,
        dbLIMIT = dbLIMIT);
    Map<String, dynamic> rChild =
        await this.filegetcontents(this.connectSiteUrl, 'post', {
      'post': this.dbType,
      'function': 'sqldb',
      'sql': s,
    });
    if (rChild['error'] == false) {
      return null;
    }
    Map<String, String> rChildArr = this.unserialize(rChild['string']);
    if (dbcell.isEmpty) {
      return '';
    }
    Map<String, String> myrow = this.unserialize(rChildArr[dbcell].toString());
    return myrow['0'].toString();
  }

  Future<Map<String, dynamic>> sqldb(String sql) async {
    return {};
  }

  String getarrayvaluedbSQL(
      [List<String>? dbcellarr,
      String dbSELECT = '*',
      String dbFROM = '',
      String dbWHERE = '',
      String dbORDER = '',
      String dbLIMIT = '']) {
    String s = '';
    if (dbSELECT.isNotEmpty) {
      s += ' SELECT ' + dbSELECT + ' ';
    } else {
      s += ' SELECT * ';
    }
    if (dbFROM.isNotEmpty) {
      s += ' FROM ' + dbFROM + ' ';
    } else {
      return 'false';
    }
    if (dbWHERE.isNotEmpty) {
      s += ' WHERE ' + dbWHERE + ' ';
    }
    if (dbORDER.isNotEmpty) {
      s += ' ORDER ' + dbORDER + ' ';
    }
    if (dbLIMIT.isNotEmpty) {
      s += ' LIMIT ' + dbLIMIT + ' ';
    }
    if (dbcellarr == null) {
      return 'false';
    } else {
      if (dbcellarr[0].isEmpty) {
        return 'false';
      }
    }
    return s;
  }

  Future<Map<String, dynamic>> getarrayvaluedb(
      [List<String>? dbcellarr,
      String dbSELECT = '*',
      String dbFROM = '',
      String dbWHERE = '',
      String dbORDER = '',
      String dbLIMIT = '']) async {
    Map<String, dynamic> param = {};
    String s = this.getarrayvaluedbSQL(
      dbcellarr = dbcellarr,
      dbSELECT = dbSELECT,
      dbFROM = dbFROM,
      dbWHERE = dbWHERE,
      dbORDER = dbORDER,
      dbLIMIT = dbLIMIT,
    );
    if (s == 'false') {
      return {'getarrayvaluedb_sql': s};
    }
    String qChild = s;
    Map<String, dynamic> rChild =
        await this.filegetcontents(this.connectSiteUrl, 'post', {
      'post': this.dbType,
      'function': 'sqldb',
      'sql': qChild,
    });
    if (rChild['error'] == false) {
      return {
        'error': rChild['error'],
        'string': rChild['string'],
        'getarrayvaluedb_sql': s,
      };
    }
    Map<String, String> rChildArr = this.unserialize(rChild['string']);
    for (int i = 0; i < dbcellarr!.length; i++) {
      String field = dbcellarr[i];
      Map<String, String> resChildArr =
          this.unserialize(rChildArr[field].toString());
      param[field] = {};
      for (int i2 = 0; i2 < resChildArr.length; i2++) {
        param[field][i2] = resChildArr[i2.toString()];
      }
    }
    param['getarrayvaluedb_sql'] = s;
    return param;
  }

  String deletedbSQL([
    String dbFROM = '',
    String dbWHERE = '',
  ]) {
    String s = '';
    if (dbFROM.isNotEmpty) {
      s += 'DELETE FROM ' + dbFROM + ' ';
    } else {
      return 'false';
    }
    if (dbWHERE.isNotEmpty) {
      s += ' WHERE ' + dbWHERE + ' ';
    }
    return s;
  }

  String deletedbSyncSQL([
    String dbFROM = '',
    String dbWHERE = '',
  ]) {
    Map<String, String> param = {};
    param['active'] = '404';
    return this.updatevaluedbSQL(dbFROM, param, dbWHERE);
  }

  Future<void> deletedb([
    String dbFROM = '',
    String dbWHERE = '',
  ]) async {
    Map<String, dynamic> param = {};
    String s = this.deletedbSQL(dbFROM = dbFROM, dbWHERE = dbWHERE);
    if (s == 'false') {
      return;
    }
    String qChild = s;
    await this.filegetcontents(this.connectSiteUrl, 'post', {
      'post': this.dbType,
      'function': 'sqldbQuery',
      'sql': qChild,
    });
  }

  String insertvaluedbSQL(String dbFROM, String dbCOLUMNS, String dbVALUES) {
    String s = 'INSERT INTO';
    if (dbFROM.isNotEmpty) {
      s += ' ' + dbFROM + ' ';
    } else {
      return 'false';
    }
    if (dbCOLUMNS.isNotEmpty) {
      s += ' (' + dbCOLUMNS + ') ';
    } else {
      return 'false';
    }
    if (dbVALUES.isNotEmpty) {
      s += ' VALUES (' + dbVALUES + ') ';
    } else {
      dbVALUES = '\'\'';
      List<String> exparray = this.explode(',', dbCOLUMNS);
      int exparraycount = exparray.length;
      for (int iii = 1; iii < exparraycount; iii++) {
        dbVALUES += ',\'\'';
      }
      s += ' VALUES (' + dbVALUES + ') ';
    }
    s += ' RETURNING id;';
    return s;
  }

  Future<Map<String, String>> addvaluedb(
      String dbFROM, Map<String, String> dbPARAM) async {
    Map<String, String> res = {};
    return res;
  }

  String addvaluedbSQL(String dbFROM, Map<String, String> dbPARAM) {
    String s = 'INSERT INTO';
    if (dbFROM.isNotEmpty) {
      s += ' ' + dbFROM + ' ';
    } else {
      return 'false';
    }
    if (dbPARAM.isNotEmpty) {
      List<String> dbPARAM_field = dbPARAM.keys.toList();
      List<String> dbPARAM_values = dbPARAM.values.toList();
      s += ' (' + implode(',', dbPARAM_field) + ') ';
      s += ' VALUES (\'' + implode('\',\'', dbPARAM_values) + '\') ';
    } else {
      return 'false';
    }
    s += ';';
    return s;
  }

  Future<Map<String, String>> updatevaluedb(
      String dbFROM, Map<String, String> dbPARAM, String dbWHERE) async {
    Map<String, String> res = {};
    return res;
  }

  String updatevaluedbSQL(
      String dbFROM, Map<String, String> dbPARAM, String dbWHERE) {
    String s = 'UPDATE ';
    if (dbFROM.isNotEmpty) {
      s += ' ' + dbFROM + ' ';
    } else {
      return 'false';
    }
    if (dbPARAM.isNotEmpty) {
      s += ' SET ';

      List<String> dbPARAM_field = dbPARAM.keys.toList();
      // List<String> dbPARAM_values = dbPARAM.values.toList();
      // s += ' (' + implode(',', dbPARAM_field) + ') ';
      // s += ' VALUES (\'' + implode('\',\'', dbPARAM_values) + '\') ';

      String ss = '';
      for (int i = 0; i < dbPARAM_field.length; i++) {
        if (ss.isEmpty) {
          ss += dbPARAM_field[i] +
              '=\'' +
              dbPARAM[dbPARAM_field[i].toString()].toString() +
              '\'';
        } else {
          ss += ', ' +
              dbPARAM_field[i] +
              '=\'' +
              dbPARAM[dbPARAM_field[i].toString()].toString() +
              '\'';
        }
      }
      s += ss;
    } else {
      return 'false';
    }
    if (dbWHERE.isNotEmpty) {
      s += ' WHERE ' + dbWHERE + ' ';
    }
    return s;
  }

  Future<Map<String, String>> savedb(
      String dbFROM, Map<String, String> dbPARAM, String dbWHERE) async {
    List<String> dbPARAM_field = dbPARAM.keys.toList();
    String? res = await this.getcelldb(dbPARAM_field[0].toString(),
        dbPARAM_field[0].toString(), dbFROM, dbWHERE, '', '1');
    Map<String, String> resArr = {};
    if ((res ?? '').isNotEmpty) {
      resArr = await this.updatevaluedb(dbFROM, dbPARAM, dbWHERE);
      resArr['action'] = 'update';
    } else {
      resArr = await this.addvaluedb(dbFROM, dbPARAM);
      resArr['action'] = 'insert';
    }
    return resArr;
  }

  String getcuts(String cuts, String http, String domenSite) {
    if (cuts == '#') {
      cuts = '/';
    }
    if (http == 'https') {
      cuts = cuts.replaceAll('http://', 'https://');
    } else {
      cuts = cuts.replaceAll('https://', 'http://');
    }
    cuts = cuts.trim();
    cuts = cuts.replaceAll('\\', '/');
    if (cuts.isEmpty) {
      return http + '://' + domenSite;
    } else {
      if (cuts.substring(cuts.length - 1) != '/') {
        cuts = cuts + '/';
      }
      // String katalcat = cuts.substring(0, 4);
      // if (katalcat == 'http') {
      //   katalcat = cuts;
      // } else {
      //   if (katalcat.substring(0, 1) == '/') {
      //     katalcat = http + '://' + domenSite + cuts;
      //   } else {
      //     katalcat = http + '://' + domenSite + '/' + cuts;
      //   }
      // }
      // return katalcat;

      if (cuts.contains('#')) {
        int pos = cuts.indexOf('#', 0);
        cuts = cuts.substring(0, pos);
      }

      String katalcat = '';
      if (cuts.trim().isEmpty) {
        katalcat = http + '://' + domenSite + cuts;
      } else if (cuts.substring(0, 1) == '/') {
        katalcat = http + '://' + domenSite + cuts;
      } else {
        try {
          katalcat = cuts.substring(0, 4);
          if (katalcat == 'http') {
            katalcat = cuts;
          } else {
            katalcat = http + '://' + domenSite + '/' + cuts;
          }
        } catch (e) {
          katalcat = cuts;
        }
      }
      return katalcat;
    }
  }

  // преобразовать телефон к следующему виду: +79999999999
  String convertPhone(String phone) {
    phone = phone.trim();
    phone = phone.replaceAll(RegExp(r'[^0-9]+'), '');
    if (phone.isEmpty) {
      return '';
    }
    if (phone[0] == '8') {
      phone = '7' + phone.substring(1);
    }
    phone = '+' + phone;
    return phone;
  }

  void message(String str, BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(str)));
  }

  void loading({bool loadingShows = true, String body = ''}) {
    this.loadingShow = loadingShows;
    notifyListeners();
  }

  int rand({int min = 0, int max = 1}) {
    Random rnd = new Random();
    int res = min + rnd.nextInt(max - min);
    return res;
  }

  List<String> mapToList(Map<String, dynamic> mapArr, String fieldName) {
    List<String> res = [];
    if (mapArr.isNotEmpty) {
      for (int i = 0; i < mapArr[fieldName].length; i++) {
        res.add(mapArr[fieldName][i]);
      }
    }
    return res;
  }

  Map<String, String> mapDynamicToString(
      Map<String, dynamic> mapArr, int numLine) {
    Map<String, String> res = {};
    List<String> fieldsArr = mapArr.keys.toList();
    for (int i = 0; i < fieldsArr.length; i++) {
      res[fieldsArr[i]] = mapArr[fieldsArr[i]][numLine].toString();
    }
    return res;
  }
}

class CookieManager {
  static CookieManager? _manager;

  static getInstance() {
    if (_manager == null) {
      _manager = CookieManager();
    }
    return _manager;
  }

  void setCookie(String key, String value) {
    // 2592000 sec = 30 days.
    document.cookie = "$key=$value; max-age=2592000; path=/;";
  }

  String getCookie(String key) {
    String? cookies = document.cookie;
    List<String> listValues = cookies!.isNotEmpty ? cookies.split(";") : [];
    String matchVal = "";
    for (int i = 0; i < listValues.length; i++) {
      List<String> map = listValues[i].split("=");
      String _key = map[0].trim();
      String _val = map[1].trim();
      if (key == _key) {
        matchVal = _val;
        break;
      }
    }
    return matchVal;
  }
}
