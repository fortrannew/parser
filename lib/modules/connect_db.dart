// этот класс позволяет выбрать, с какой базой данных работать, к какой подключиться и пр.
//
// - Синхронизация данных между локальной и глобальной БД:
// import 'lcp/lcp_sync_db.dart';
// final lcp = new SyncDB();
//
// - Работать только с локальной БД
// import 'lcp/lcp_connect_local.dart';
// final lcp = new LcpLocal('sqlite');
//
// - Работать только с глобальной БД
// import 'lcp/lcp_connect_remote.dart';
// final lcp = new LcpRemote('mysql');
//
// - Работать только с web-приложением
// import 'lcp/lcp_db.dart';
// final lcp = new LCPdb();

import 'lcp/lcp_sqlite.dart';
import '../konsfig.dart';
import '../create_db_sqlite.dart';

final lcp = LCPSQLite(
    dbPath: connectDBPathLocal,
    applicationName: applicationName,
    createDB: createSQLite);
